package dao;

import hibernate.HibernateSessionFactoryUtil;

import java.io.Serializable;
import java.util.List;

public class GenericDaoImpl<T, PK extends Serializable> implements GenericDao<T, PK> {

    private Class<T> type;

    public GenericDaoImpl(Class<T> type) {
        this.type = type;
    }

    public T getById(PK id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(this.type, id);
    }

    public List<T> getAll() {
        List<T> objects = HibernateSessionFactoryUtil.getSessionFactory()
                .openSession().createQuery("From " + this.type.getName()).list();
        return objects;
    }
}


