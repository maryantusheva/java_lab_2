package dao;

import dao.GenericDaoImpl;
import models.*;

import javax.persistence.Temporal;
import java.util.List;

public class Dao {
    GenericDaoImpl implem;
    public Client getClient(Integer id) {
        implem = new GenericDaoImpl(Client.class);
        return (Client) implem.getById(id);
    }
    public List<Client> getClients () {
        implem = new GenericDaoImpl(Client.class);
        return implem.getAll();
    }

    public Employee getEmployee(Integer id) {
        implem = new GenericDaoImpl(Employee.class);
        return (Employee) implem.getById(id);
    }
    public List<Employee> getEmployees () {
        implem = new GenericDaoImpl(Employee.class);
        return implem.getAll();
    }

    public Service getService(Integer id) {
        implem = new GenericDaoImpl(Service.class);
        return (Service) implem.getById(id);
    }
    public List<Service> getServices () {
        implem = new GenericDaoImpl(Service.class);
        return implem.getAll();
    }

    public Appointment getAppointment(Integer id) {
        implem = new GenericDaoImpl(Appointment.class);
        return (Appointment) implem.getById(id);
    }
    public List<Appointment> getAppointments () {
        implem = new GenericDaoImpl(Appointment.class);
        return implem.getAll();
    }

}