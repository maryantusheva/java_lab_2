import dao.Dao;
import hibernate.HibernateSessionFactoryUtil;

import java.util.Formatter;
import java.util.Scanner;

import static java.lang.System.exit;
import static java.lang.System.in;

public class Main {

    public static void main(String[] args) {
        Dao dao = new Dao();
        System.out.println("\n_____________________ All services _____________________");
        System.out.println(dao.getServices());
        System.out.println("_____________________ All clients _____________________");
        System.out.println(dao.getClients());
        System.out.println("\n_____________________ All employees _____________________");
        System.out.println(dao.getEmployees());
        System.out.println("\n_____________________ All appointments _____________________");
        System.out.println(dao.getAppointments());
        System.out.println("\n_____________________ Client by id #1 _____________________");
        System.out.println(dao.getClient(1));
        System.out.println("\n_____________________ Client by id #1 get appointments _____________________");
        System.out.println(dao.getAppointment(dao.getClient(1).getId()));

/*
        System.out.println("\n_____________________ All employees _____________________");
        System.out.println(dao.getEmployees());
        System.out.println("\n_____________________ Employee by id #2 _____________________");
        System.out.println(dao.getEmployee(2));
        //System.out.println(dao.getService(dao.getEmployee(2).getServiceId()));

        System.out.println("\n_____________________ All services _____________________");
        System.out.println(dao.getServices());
        System.out.println("\n_____________________ Service by id #2 _____________________");
        System.out.println(dao.getService(2));

        System.out.println("\n_____________________ All appointments _____________________");
        System.out.println(dao.getAppointments());
        System.out.println("\n_____________________ App by id #2 _____________________");
        System.out.println(dao.getAppointment(2));
*/

        HibernateSessionFactoryUtil.getSessionFactory().close();

    }

    public int menu() {
        int selection;
        Scanner input = new Scanner(System.in);
        /***************************************************/
        System.out.println("Choose from these choices");
        System.out.println("-------------------------\n");
        System.out.println(" 1 - Client`s submenu");
        System.out.println(" 2 - Employee`s submenu");
        System.out.println(" 3 - Appointment`s submenu");
        System.out.println(" 4 - Quit");
        selection = input.nextInt();
        return selection;
    }
}

