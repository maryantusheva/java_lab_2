package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity(name = "services")
public class Service implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    private Integer id;

    /*
    One type of service can make a few employees
     */

    @OneToMany(mappedBy = "mainService", fetch = FetchType.EAGER)
    private List<Employee> masters;

    /*
    One service can be written in a few appointments
     */

    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    private List<Appointment> appointments_records;

    @Column
    private String type;

    @Column
    private Integer duration;

    @Column
    private Integer price;


    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }
    public void setFaculty(Integer price) {
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Service() { }

    public Service(Integer id, String type, Integer duration,
                  Integer price) {
        this.id = id;
        this.type = type;
        this.duration = duration;
        this.price = price;
    }
    @Override
    public String toString() {
        return "\n Service id : " + id + "  | " +
                "type : " + type  + " | " +
                "price : " + price  + " | " +
                "duration : "  + duration + " | ";
    }
}
