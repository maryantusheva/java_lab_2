package models;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Collection;
import java.util.Date;
import java.util.Set;


@Entity(name = "appointments")
public class Appointment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, nullable = false)
    private Integer id;

    /*
     One appointment can be made only by one client,
     but at the same time the client can have several appointments.
    */
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id")
    private Client client;

    /*
     One appointment task can be performed only by one employee,
     but at the same time the employee can have several appointment`s tasks.
    */

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "emp_id")
    private Employee employee;

    /*
    There can only be one service in one record,
    but there can be many records for a service.
    */

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "service_id")
    private Service service;

    @Column
    private String record_date;

    @Column
    private String  record_time;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) { this.id = id; }

    public String getDate() {
        return record_date;
    }
    public void setDate(String record_date) {
        this.record_date = record_date;
    }

    public String getTime() {
        return  record_time;
    }
    public void setTime(String record_time) {
        this.record_time = record_time;
    }

    public Integer getClientId() { return client.account_id; }
    public void setClientId(Integer clientRecordId) { clientRecordId = client.getId(); }

    public Integer getEmployeeId() {
        return employee.getId();
    }
    public void setEmployeeId(Integer recordEmployeeId) { recordEmployeeId = employee.getId(); }

    public Integer getServiceId() {
        return service.getId();
    }
    public void setServiceId(Integer serviceId) { serviceId = service.getId();
    }
    public Appointment(){};
    public Appointment(Integer id, Integer client_id, Integer emp_id,
                       Integer service_id, String record_date, String record_time) {
        this.id = id;
        this.record_date = record_date;
        this.record_time = record_time;
    }
    @Override
    public String toString() {
        return "\n Appointment id : " + this.getId() + "  | " +
                "date : " + this.getDate()  + " | " +
                "time : " + this.getTime()  +  "|";
               // "client : " +   +  "|" +
               // "employee  : " + this.getEmployeeId() +  "|"+
               // "service  : " + this.getServiceId()  +  "|";
    }
}
