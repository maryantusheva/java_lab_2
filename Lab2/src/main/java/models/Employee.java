package models;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="employees")
public class Employee extends User {
    /*
    It is obvious that the employee provides one its main type of service, but a service can make a few employees.
    Employees and Services form a one-to-many relationship
     */
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "service_id")
    private Service mainService;

    /*
    One employee can make a few appointments
     */
    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    private List<Appointment> appointments_records;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "category")
    private String category;

    public Employee() { }

    public Employee(Integer account_id, String full_name, String username,
                    String password_hash, String birthday,
                    Integer rating, String category) {
        super(account_id, full_name,  username,
                password_hash, birthday);
        this.rating = rating;
        this.category = category;
    }

    public Integer getServiceId() {
        return mainService.getId();
    }
    public void setServiceId(Integer mainServiceId) { mainServiceId = mainService.getId();}

    public String toString() {
        return " \n Employee id : " + this.getId() + "  | " +
                "fullName : " + this.getFullName()  + " | " +
                "username : " + this.getUsername()  + " | " +
                "password_hash : "  + this.getPasswordHash()+ " | " +
                "dateOfBirth : " + this.getBirthday()  + " | " +
                "rating : " + this.rating + " | " +
                "category : "  + this.category + " | ";
            //    "service : "  + mainService.getId() + " | ";
    }

}
