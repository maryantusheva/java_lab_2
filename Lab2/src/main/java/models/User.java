package models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Formatter;
@MappedSuperclass
public abstract class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id", updatable = false, nullable = false)
    protected Integer account_id;

    @Column(name = "full_name")
    private String full_name;

    @Column(name = "username")
    private String username;

    @Column(
            name = "password_hash"
    )
    private String password_hash;
    @Column(
            name = "birthday"
    )
    private String birthday;

    User(Integer account_id, String full_name, String username,
         String password_hash,  String birthday) {
        this.account_id = account_id;
        this.full_name = full_name;
        this.username = username;
        this.password_hash = password_hash;
        this.birthday = birthday;
    }

    public User() {
    }

    public Integer getId() {
        return account_id;
    }

    public void setId(Integer id) {
        this.account_id = account_id;
    }

    public String getFullName() {
        return full_name;
    }

    public void setFirstName(String firstName) {
        this.full_name = full_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() { return this.password_hash; }

    public void setPasswordHash(String password_hash) { this.password_hash = password_hash; }

    public String getBirthday() { return this.birthday; }

    public void setBirthday(String dateOfBirth) { this.birthday = birthday; }
    }

