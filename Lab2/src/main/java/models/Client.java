package models;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="clients")
public class Client extends User {

    /*
    One client can make a few appointments
     */
    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    private List<Appointment> appointments_records;

    @Column(
            name = "phone_number"
    )
    private String phone_number;
    @Column(name = "email")
    private String email;

    public Client() {
    }
     public Client(Integer account_id, String full_name, String username,
                   String password_hash,  String  birthday,
                   String phone_number, String email) {
        super(account_id, full_name, username,
                password_hash, birthday);
        this.phone_number = phone_number;
        this.email = email;
    }
    public String toString() {
        return " \n Client id : " + this.getId() + "  | " +
                "fullName : " + this.getFullName()  + " | " +
                "username : " + this.getUsername()  + " | " +
                "password_hash : "  + this.getPasswordHash()+ " | " +
                "dateOfBirth : " + this.getBirthday()  + " | " +
                "phone_number=" + this.phone_number  + "| " +
                "email=" + this.email + "| ";
    }
}
