import dao.*;
import models.*;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DaoTests {

    private Dao dao;

    public DaoTests() throws SQLException {
        dao = new Dao();

        dao = new Dao();
    }

    @Test
    public void getClientByIdDaoRest() {
        Client expected = new Client(1, "Антушева Марія", "maria",
                "dhjsie74", "1997-12-17", "380987654345",
                "antusheva@gmail.com");
        Client actual = dao.getClient(1);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void getEmployeeByIdDaoRest() throws ParseException {
        Employee expected = new Employee(3, "Якименко Максим", "maximuss",
                "g283hfji", "1991-01-03", 7,  "cosmetologist");
        Employee actual = dao.getEmployee(3);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void getServiceByIdDaoRest() {
        Service expected = new Service(1, "french manicure", 1, 1500 );
        Service actual = dao.getService(1);
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void getAppointmentByIdDaoRest() {
        Appointment expected = new Appointment(1, 1, 3, 1, "2019-11-22", "10:20:00");
        Appointment actual = dao.getAppointment(1);
        assertEquals(expected.toString(), actual.toString());
    }
}